function database = add2base(database,fingerprint)
    database(length(database)+1) = {fingerprint};
end